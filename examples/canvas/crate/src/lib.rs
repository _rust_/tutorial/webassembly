use std::f64;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::CanvasRenderingContext2d;
use web_sys::Document;

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    create_canvas(&document)?;

    let context = get_canvas_context(&document, "canvas")?;

    draw_smiley(&context);

    Ok(())
}

pub fn create_canvas(document: &Document) -> Result<(), JsValue> {
    let body = document.body().expect("document should have a body");

    let canvas: web_sys::Element = document.create_element("canvas")?;
    canvas.set_attribute("height", "150").unwrap();
    canvas.set_attribute("width", "150").unwrap();
    canvas.set_attribute("id", "canvas").unwrap();
    body.append_child(&canvas)?;

    Ok(())
}

pub fn get_canvas_context(
    document: &Document,
    canvas_id: &str,
) -> Result<CanvasRenderingContext2d, JsValue> {
    let canvas = document.get_element_by_id(canvas_id).unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<CanvasRenderingContext2d>()
        .unwrap();

    Ok(context)
}

pub fn draw_smiley(context: &CanvasRenderingContext2d) {
    context.begin_path();

    // Draw the outer circle.
    context
        .arc(75.0, 75.0, 50.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();

    // Draw the mouth.
    context.move_to(110.0, 75.0);
    context.arc(75.0, 75.0, 35.0, 0.0, f64::consts::PI).unwrap();

    // Draw the left eye.
    context.move_to(65.0, 65.0);
    context
        .arc(60.0, 65.0, 5.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();

    // Draw the right eye.
    context.move_to(95.0, 65.0);
    context
        .arc(90.0, 65.0, 5.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();

    context.stroke();
}
